#!/usr/bin/env bash

set -e

# Need to copy to post script in /tmp, because docker compose doesn't support variables
# in a key, so not able to create a volume like -"${POST_SCRIPT}:/container_path"
SCRIPTPATH=$(dirname "$(readlink -f "$0")")
POST_SCRIPT="${SCRIPTPATH}/post_script"
#Union all SQLs and add to shared single sql file if files are exist
if ls ${POST_SCRIPT}/*.sql >/dev/null 2>&1; then
    echo "SQL Files exist"
    cat ${POST_SCRIPT}/*.sql > /tmp/postscript.sql
else
    echo "SQL Files do not exist"
    touch /tmp/postscript.sql
fi

export NUM_MANAGER_NODES=1
export NODE_ROLE=manager
export DISK_DRIVER=local
# for production use please change log level to warn or error. Debug will generate a lot of information.
# possible values ERROR, WARN, INFO, DEBUG, or TRACE
export LOGGING_LEVEL="WARN"

# if set to 'debug' or 'trace', bash scripts will execute with the -x flag enabled (i.e. prints every command and argument)
export OPENIAM_BASH_LOG_LEVEL=warn
if [ ! -z "$OPENIAM_LOG_LEVEL" ] && [ "$OPENIAM_LOG_LEVEL" == "debug" ] || [ "$OPENIAM_LOG_LEVEL" == "trace" ]
then
  set -x
fi
 
export ELASTICSEARCH_DISCOVERY_URL=tasks.elasticsearch_service

export OPENIAM_VERSION_NUMBER=${OPENIAM_VERSION_NUMBER:-"4.2.1.12"}
export BUILD_ENVIRONMENT="prod"
export CONTAINER_REGISTRY="${CONTAINER_REGISTRY:-confidant-bhaskara.container-registry.com}"
export CONTAINER_INFRA_NAMESPACE="openiam_infra"
export CONTAINER_SERVICE_NAMESPACE="${CONTAINER_SERVICE_NAMESPACE:-openiam_service}"
#export CONTAINER_SERVICE_NAMESPACE="openiam_service_ce"

export RABBITMQ_USERNAME="openiam"
export RABBITMQ_PASSWORD="Password#51"
export REDIS_PASSWORD="passwd00"
export REDIS_SENTINEL_PASSWORD="passwd00"
export DB_ROOT_PASSWORD="passwd00"
export CASSANDRA_PASSWORD="passwd00"
export DB_TYPE="MariaDB"
export LDAP_KEYSTORE_PASSWORD="changeit"
export JDBC_OPENIAM_DB_USER=IAMUSER
export JDBC_OPENIAM_DB_PASSWORD=IAMUSER
export JDBC_ACTIVITI_DB_USER=ACTIVITI
export JDBC_ACTIVITI_DB_PASSWORD=ACTIVITI
export OPENIAM_DATABASE_NAME=openiam
export JDBC_SCHEMA_NAME=openiam
export ACTIVITI_DATABASE_NAME=activiti
export JDBC_ACTIVITI_SCHEMA_NAME=activiti
export ELASTICDUMP_ENABLED="false"

#uncomment following section if you use external DB
#export JDBC_HOST=hostname
#export JDBC_PORT=port

##### special section for oracle
#export JDBC_SID=SID
#export JDBC_SERVICE_NAME=SERVICE_NAME
#export OPENIAM_PROP_user_timezone=ORA_TIME_ZONE
#export OPENIAM_HIBERNATE_DIALECT="org.hibernate.dialect.Oracle10gDialect"

##### special section for mssql
#export OPENIAM_HIBERNATE_DIALECT="org.hibernate.dialect.SQLServer2012Dialect"

##### special section for postgres
#export OPENIAM_HIBERNATE_DIALECT="org.hibernate.dialect.PostgreSQL95Dialect"

#property can be overwritten in ui docker-compose.yaml
export REDIS_USE_CLUSTER=false

# The OPENIAM_RPROXY_SERVER_NAME will be used in ServerName apache config option
# for example:
# export OPENIAM_RPROXY_SERVER_NAME=www.example.com
export OPENIAM_RPROXY_SERVER_NAME=


# rproxy by default uses https. you can set OPENIAM_RPROXY_HTTP=1 to disable https
export OPENIAM_RPROXY_HTTP=1

# SSL certificate file names (without full path, just filename)
# Certificates should be in /opt/openiam/httpd/ssl-certs directory
export OPENIAM_SSL_CERT=

# SSL certificate key file name (without full path, just filename)
# Certificate key should be in /opt/openiam/httpd/ssl-keys directory
export OPENIAM_SSL_CERT_KEY=

# SSL CA certificate. use one of this environment variables: OPENIAM_SSL_CHAIN or OPENIAM_SSL_CA
#export OPENIAM_SSL_CHAIN=
export OPENIAM_SSL_CA=


# SSL Protocols and Ciphers options. if not set, apache 2.4 defaults will be used.
# For Example: disable everything except TLSv1.2 and allow only ciphers with high encryption:
#export OPENIAM_SSL_PROTOCOL="-ALL -TLSv1 -TLSv1.1 +TLSv1.2"
#export OPENIAM_SSL_CIPHER_SUITE="HIGH:!MEDIUM:!aNULL:!MD5:!RC4"
export OPENIAM_SSL_PROTOCOL=
export OPENIAM_SSL_CIPHER_SUITE=

# Ask for client certificate from browser. Correspond to SSLVerifyClient config option in apache
# Set it to optional_no_ca.
#export OPENIAM_SSL_VERIFY_CLIENT=optional_no_ca

# if https uses non-default(443) port, specify full https host name for redirects from http to https.
# for example:
# export OPENIAM_HTTPS_HOST=https://www.example.com:8001
export OPENIAM_HTTPS_HOST=


# Apache mod_deflate compression ratio. Values from 0 to 9. 0 - No compression, 9 - Maximum compression
# By default set to 6
export OPENIAM_RPROXY_DEFLATE=6


# rproxy logging
# By default /dev/stderr used for OPENIAM_RPROXY_ERROR_LOG and
# /dev/stdout for OPENIAM_RPROXY_ACCESS_LOG. You can change that here,
# for example use /dev/stdout for both error log and access log
# or set access log to /dev/null and log only errors.
# if not set, defaults used.
#export OPENIAM_RPROXY_ERROR_LOG=/dev/stderr
#export OPENIAM_RPROXY_ACCESS_LOG=/dev/stdout

# rproxy debug options. set any value for enable or keep empty for disable debug logging
export OPENIAM_RPROXY_DEBUG=0
export OPENIAM_RPROXY_VERBOSE=0
export OPENIAM_RPROXY_DEBUG_ESB=0
export OPENIAM_RPROXY_DEBUG_AUTH=0

# rproxy will dump all request and response headers as plain text together with requests POST bodies.
# it is insecure to use this config option
export OPENIAM_RPROXY_DUMP_ALL=0

# rproxy configs for OpenIAM core services.
# Use http://esb:9080 for rproxy in dockerized environment (default)
# or set http://localhost:9080 for standalone setup
export OPENIAM_RPROXY_ESB=http://esb:9080

# rproxy configs for UI.
# Use http://ui:8080 for rproxy in dockerized environment (default)
# or set http://localhost:8080 for standalone setup
export OPENIAM_RPROXY_UI=http://ui:8080

# If user hit for example: http://demo.openiamdemo.com/ redirect him to
# http://demo.openiamdemo.com/selfservice/ by default, instead of showing error 404 (not found).
export OPENIAM_DEFAULT_URI=/selfservice/

# OPENIAM_DISABLE_CONFIGURE set to 1 by default. this allow to configure content provider on first access.
# But after that, it is possible to set it to 0 to disable ability to configure content provider using /webconsole/setup.html url.
OPENIAM_DISABLE_CONFIGURE=0

# Content-Security-Policy headers enabled by default. To disable set to '0'
export OPENIAM_RPROXY_CSP=1

# Cross-Origin Resource Sharing by default set to 1. To disable set to '0'
export OPENIAM_RPROXY_CORS=1

# This environment variables allow to include some config in apache running in rproxy's docker container.
# this should be file name without full path, just filename. this files should be stored in /opt/openiam/httpd/conf-add directory
# Uncomment this line in rproxy/docker-compose.yaml or rproxy-ssl/docker-compose.yaml for use this config options:
# - /opt/openiam/httpd/conf-add:/usr/local/apache2/conf/add
export OPENIAM_APACHE_EXTRA_CONF=
# The same but included in VirtualHost section in mod_openiam_https.conf
export OPENIAM_VHOST_EXTRA_CONF=

#This environment variables allow to redirect syslogs from OpenIAM to Docker Host server
export SYS_LOG_HOST=host.openiam
#Use OS command to get IP or set it manualy. For example :
#export DOCKER_HOST_IP=172.18.0.1
# or
# export DOCKER_HOST_IP=$(ifconfig docker_gwbridge |awk '/inet/ {print $2}')  (example for centos 7 OS)
# default value (127.0.0.1) will not redirect logs to docker host
export DOCKER_HOST_IP=127.0.0.1

#If required you are able to change response timeout on redisson. Change the following property
export REDISSON_TIMEOUT="5000"

# If you are running on a development box which you do not have sudo to, consider setting this property.
# This essentially disabled Elasticsearch bootstrap Operating System checks, which you have no control over as a non-privileged user
export ELASTICSEARCH_DISCOVERY_TYPE="single-node"

#data analyzing tools:
#kibana tool is enabled if OPENIAM_MONITORING_TOOLS_ENABLED =1, change to 0 to disable
export OPENIAM_MONITORING_TOOLS_ENABLED=0
export OPENIAM_MONITORING_TOOLS_VERSION="7.16.2"

#SMS OTP config
export SMS_GLOBAL_API_KEY="changeit"
export SMS_GLOBAL_SECRET="changeit"
#
# put your custom environment variables here
#

# properties for flyway.  Make sure to modify these!

# is flyway enabled?
export FLYWAY_ENABLED=1

# if this is a new install, leave this as 2.3.0.0. If you are upgrading from pre-4.2.0, set this to the next point
# version.  For example, if you are upgrading from 4.1.5, set this value to 4.1.5.1.  If upgrading from 4.1.5.1,
# set this to 4.1.5.2
export FLYWAY_BASELINE_VERSION=2.3.0.0

# name of the activiti database.  If using mariadb or postgres, this is likely 'activiti'
export FLYWAY_ACTIVITI_DATABASE_NAME=$ACTIVITI_DATABASE_NAME

# name of the openiam core database.  If using mariadb or postgres, this is likely 'openiam'
export FLYWAY_OPENIAM_DATABASE_NAME=$OPENIAM_DATABASE_NAME

# port of the activiti database.  If using mariadb, this is likely '3306'.  If using postgres, this is likely '5432'
export FLYWAY_ACTIVITI_PORT=3306

# host of the activiti database.  If using mariadb or postgres in docker, this is likely 'database'
export FLYWAY_ACTIVITI_HOST=database

# port of the openiam database.  If using mariadb, this is likely '3306'.    If using mariadb, this is likely '3306'.  If using postgres, this is likely '5432'
export FLYWAY_OPENIAM_PORT=3306

# host of the openiam database.  If using mariadb or postgres in docker, this is likely 'database'
export FLYWAY_OPENIAM_HOST=database

# username to activiti database.  In postgres, this must be a superuser
export FLYWAY_ACTIVITI_USERNAME=$JDBC_ACTIVITI_DB_USER

# password to activiti database
export FLYWAY_ACTIVITI_PASSWORD=$JDBC_ACTIVITI_DB_PASSWORD

# username to openiam database.  In postgres, this must be a superuser
export FLYWAY_OPENIAM_USERNAME=$JDBC_OPENIAM_DB_USER

# password to openiam database
export FLYWAY_OPENIAM_PASSWORD=$JDBC_OPENIAM_DB_PASSWORD

# Vault configuration

# set this to 'false' to enable auto-unsealing of vault.  It is recommended to leave this value as-is
export VAULT_SECURE_DEPLOYMENT=false

# set this variable after generating the jks file.  See README for instructions
export VAULT_JKS_PASSWORD=passwd00

# leave as unchanged if you are uprading from pre-4.2.0
export KEYSTORE_PSWD=changeit
export IAM_JKS_PASSWORD=openiamKeyStorePassword
export IAM_JKS_KEY_PASSWORD=openiamMasterKey
export IAM_JKS_COOKIE_KEY_PASSWORD=openiamCookieKey
export IAM_JKS_COMMON_KEY_PASSWORD=openiamCommonKey

# SAS Servers configuration
export SAS_PRIMARY_PROTOCOL=http
export SAS_PRIMARY_SERVER=
export SAS_PRIMARY_PORT=80
export SAS_SECONDARY_PROTOCOL=http
export SAS_SECONDARY_SERVER=
export SAS_SECONDARY_PORT=80

# leave a line break at the end of this file
