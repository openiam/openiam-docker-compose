#!/usr/bin/env bash

set -e

. env.sh

if [ -f /opt/openiam/webapps/env.sh ]
then
  . /opt/openiam/webapps/env.sh
fi

if [ ! -z "$OPENIAM_LOG_LEVEL" ] && [ "$OPENIAM_LOG_LEVEL" == "debug" ] || [ "$OPENIAM_LOG_LEVEL" == "trace" ]
then
  set -x
fi

: "${RABBITMQ_PASSWORD:?RABBITMQ_PASSWORD must be set as an environment variable.}"
: "${REDIS_PASSWORD:?REDIS_PASSWORD must be set as an environment variable.}"
: "${LDAP_KEYSTORE_PASSWORD:?LDAP_KEYSTORE_PASSWORD must be set as an environment variable.}"
: "${JDBC_OPENIAM_DB_USER:?JDBC_OPENIAM_DB_USER must be set as an environment variable.}"
: "${JDBC_OPENIAM_DB_PASSWORD:?JDBC_OPENIAM_DB_PASSWORD must be set as an environment variable.}"
: "${JDBC_ACTIVITI_DB_USER:?JDBC_ACTIVITI_DB_USER must be set as an environment variable.}"
: "${JDBC_ACTIVITI_DB_PASSWORD:?JDBC_ACTIVITI_DB_PASSWORD must be set as an environment variable.}"
: "${KEYSTORE_PSWD:?KEYSTORE_PSWD must be set as an environment variable.}"
: "${IAM_JKS_PASSWORD:?IAM_JKS_PASSWORD must be set as an environment variable.}"
: "${IAM_JKS_KEY_PASSWORD:?IAM_JKS_KEY_PASSWORD must be set as an environment variable.}"
: "${IAM_JKS_COOKIE_KEY_PASSWORD:?IAM_JKS_COOKIE_KEY_PASSWORD must be set as an environment variable.}"
: "${IAM_JKS_COMMON_KEY_PASSWORD:?IAM_JKS_COMMON_KEY_PASSWORD must be set as an environment variable.}"
: "${VAULT_JKS_PASSWORD:?VAULT_JKS_PASSWORD must be set as an environment variable.}"

if [ "${FLYWAY_ENABLED}" -eq 1 ]; then
    : "${FLYWAY_ACTIVITI_PORT:?FLYWAY_ACTIVITI_PORT must be set as an environment variable.}"
    : "${FLYWAY_ACTIVITI_HOST:?FLYWAY_ACTIVITI_HOST must be set as an environment variable.}"
    : "${FLYWAY_OPENIAM_PORT:?FLYWAY_OPENIAM_PORT must be set as an environment variable.}"
    : "${FLYWAY_OPENIAM_HOST:?FLYWAY_OPENIAM_HOST must be set as an environment variable.}"
    : "${FLYWAY_ACTIVITI_USERNAME:?FLYWAY_ACTIVITI_USERNAME must be set as an environment variable.}"
    : "${FLYWAY_ACTIVITI_PASSWORD:?FLYWAY_ACTIVITI_PASSWORD must be set as an environment variable.}"
    : "${FLYWAY_OPENIAM_USERNAME:?FLYWAY_OPENIAM_USERNAME must be set as an environment variable.}"
    : "${FLYWAY_OPENIAM_PASSWORD:?FLYWAY_OPENIAM_PASSWORD must be set as an environment variable.}"
fi

cd 3.2

echo "Using $DB_TYPE as the database type..."

docker stack rm flyway
sleep 5

docker volume create etcd_storage
docker volume create vault_server_storage
docker volume create vault_client_storage
docker volume create connector_data_storage
docker volume create filebeat-storage
docker volume create openiam-janusgraph-storage
docker volume create upload_storage

# change volume permission
docker run -it -v etcd_storage:/etcd busybox sh -c 'mkdir -p /etcd/data && chown -R 1001 /etcd'
docker run -it -v upload_storage:/data/openiam/upload  busybox sh -c 'chown -R 29837 /data/openiam/upload'

# copy certs into server vault directory
docker run -it -v vault_server_storage:/data/openiam/conf/vault/server \
               -v "$(pwd)/../vault.crt:/tmp/vault.crt" \
               -v "$(pwd)/../vault.key:/tmp/vault.key" \
               -v "$(pwd)/../vault.ca.key:/tmp/vault.ca.key" \
               busybox sh -c 'mkdir -p /data/openiam/conf/vault/server && cp -r /tmp/vault* /data/openiam/conf/vault/server && chown -R 100 /data/openiam/conf/vault/server && chmod 744 /data/openiam/conf/vault/server/*'

# copy certs into client vault directory
docker run -it -v vault_client_storage:/data/openiam/conf/vault/client \
               -v "$(pwd)/../vault.jks:/tmp/vault.jks" \
               busybox sh -c 'mkdir -p /data/openiam/conf/vault/client && cp -r /tmp/vault.jks /data/openiam/conf/vault/client/vault.jks && chown -R 29837 /data/openiam/conf/vault/client'

# set perms on seal storage
docker run -it -v vault_seal_storage:/data/openiam/conf/vault/seal \
               busybox sh -c 'mkdir -p /data/openiam/conf/vault/seal && chown -R 100 /data/openiam/conf/vault/seal'

# required for filebeat.
docker run -it -v "$(pwd)/infrastructure/filebeat/filebeat.yml:/tmp/filebeat.yml" busybox sh -c 'chown -R 0 /tmp/filebeat.yml && chmod go-w /tmp/filebeat.yml'

docker stack deploy --compose-file infrastructure/etcd/docker-compose.yaml --with-registry-auth etcd

sleep 10

docker stack deploy --compose-file infrastructure/vault/docker-compose.yaml --with-registry-auth vault

sleep 20

docker stack deploy --compose-file utilities/vault/docker-compose.yaml --with-registry-auth vault-bootstrap

sleep 10

docker stack deploy --compose-file utilities/curator/docker-compose.yaml --with-registry-auth curator

if [ "$VAULT_SECURE_DEPLOYMENT" == "true" ]; then
    echo "This is a secure vault deployment.  You need to unseal the vault before continuing."
    while true
    do
    	vault_id=`docker ps | grep "vault" | awk '{ print $1 }' | head -n 1`
    	sealed=`docker exec -it $vault_id vault status -format "json" | jq .sealed`
    	if [ "$sealed" == "true" ]; then
    	    echo "Vault is still sealed.  Waiting..."
    	    sleep 10
    	else
    	    echo "Vault has been unsealed. Continuing"
    	    break;
    	fi
    done
fi

# setup volumes
docker stack deploy --compose-file metadata/docker-compose.yaml --with-registry-auth openiam-elasticsearch-storage
docker stack deploy --compose-file metadata/docker-compose.yaml --with-registry-auth openiam-jks-storage
docker stack deploy --compose-file metadata/docker-compose.yaml --with-registry-auth openiam-activiti-storage
docker stack deploy --compose-file metadata/docker-compose.yaml --with-registry-auth openiam-rabbitmq-storage
docker stack deploy --compose-file metadata/docker-compose.yaml --with-registry-auth openiam-iamscripts-storage

# deploy infrastructure services
docker stack deploy --compose-file infrastructure/redis/docker-compose.yaml --with-registry-auth redis
docker stack deploy --compose-file infrastructure/elasticsearch/docker-compose.yaml --with-registry-auth elasticsearch
docker stack deploy --compose-file infrastructure/cassandra/docker-compose.yaml --with-registry-auth cassandra


if [ "$ELASTICDUMP_ENABLED" == "true" ]; then
  docker volume create openiam-elasticdump-storage_storage
  docker stack deploy --compose-file utilities/elasticdump/docker-compose.yaml --with-registry-auth elasticdump
fi

echo "Waiting for cassandra to become running, so that we can bring up janusgraph"
sleep 60

docker stack deploy --compose-file infrastructure/janusgraph/docker-compose.yaml --with-registry-auth janusgraph
docker stack deploy --compose-file infrastructure/rabbitmq/docker-compose.yaml --with-registry-auth rabbitmq

#deploy data analyzing tools
 if [[ "$OPENIAM_MONITORING_TOOLS_ENABLED" -eq 1 ]]; then
    envsubst < infrastructure/metricbeat/metricbeat.yml > infrastructure/metricbeat/metricbeat.deploy

    # required for metricbean.
    docker run -it -v "$(pwd)/infrastructure/metricbeat/metricbeat.deploy.yml:/tmp/metricbeat.deploy.yml" busybox sh -c 'chown -R 0 /tmp/metricbeat.deploy.yml && chmod go-w /tmp/metricbeat.deploy.yml'
    docker stack deploy --compose-file infrastructure/kibana/docker-compose.yaml --with-registry-auth kibana
#    docker stack deploy --compose-file infrastructure/metricbeat/docker-compose.yaml --with-registry-auth metricbeat
    docker stack deploy --compose-file infrastructure/filebeat/docker-compose.yaml --with-registry-auth filebeat
fi

if [[ "$DB_TYPE" == 'MariaDB' ]]; then
    docker volume create openiam-mysql-storage_storage
    docker run -it -v openiam-mysql-storage_storage:/bitnami/mariadb busybox sh -c 'chown -R 1001:1001 /bitnami/mariadb'
	  docker stack deploy --compose-file infrastructure/mariadb/docker-compose.yaml --with-registry-auth database
fi

if [[ "$DB_TYPE" == 'Postgres' ]]; then
    docker volume create openiam-postgres-storage_storage
	  docker stack deploy --compose-file infrastructure/postgres/docker-compose.yaml --with-registry-auth database
fi

if [ "${FLYWAY_ENABLED}" -eq 1 ]; then
    docker stack deploy --compose-file utilities/flyway/docker-compose.yaml --with-registry-auth flyway
    sleep 60
fi

# deploy the OpenIAM Stack
docker stack deploy --compose-file services/docker-compose.yaml --with-registry-auth openiam
# deploy sas-manager module
#docker stack deploy --compose-file services-sas/docker-compose.yaml --with-registry-auth sas-manager
# deploy the OpenIAM UI
docker stack deploy --compose-file ui/docker-compose.yaml --with-registry-auth ui

# deploy the LDAP Connector
docker stack deploy --compose-file connectors/ldap/docker-compose.yaml --with-registry-auth ldap-connector
# deploy the Google Connector
#docker stack deploy --compose-file connectors/google/docker-compose.yaml --with-registry-auth google-connector
# deploy the Linux Connector
#docker stack deploy --compose-file connectors/linux/docker-compose.yaml --with-registry-auth linux-connector
# deploy the AWS Connector
#docker stack deploy --compose-file connectors/aws/docker-compose.yaml --with-registry-auth aws-connector
# deploy the Script Connector
#docker stack deploy --compose-file connectors/script/docker-compose.yaml --with-registry-auth script-connector
# deploy the Oracle Connector
#docker stack deploy --compose-file connectors/oracle/docker-compose.yaml --with-registry-auth oracle-connector
# deploy the Oracle EBS Connector
#docker stack deploy --compose-file connectors/oracle-ebs/docker-compose.yaml --with-registry-auth oracle-ebs-connector
# deploy the SCIM Connector
#docker stack deploy --compose-file connectors/scim/docker-compose.yaml --with-registry-auth scim-connector
# deploy the Oracle IDCS Connector
#docker stack deploy --compose-file connectors/oracle-idcs/docker-compose.yaml --with-registry-auth oracle-idcs-connector
# deploy the Tableau Connector
#docker stack deploy --compose-file connectors/tableau/docker-compose.yaml --with-registry-auth tableau-connector
# deploy the ADP Connector
#docker stack deploy --compose-file connectors/adp/docker-compose.yaml --with-registry-auth adp-connector
# deploy the IPA Connector
#docker stack deploy --compose-file connectors/ipa/docker-compose.yaml --with-registry-auth ipa-connector
# deploy the Salesforce Connector
#docker stack deploy --compose-file connectors/salesforce/docker-compose.yaml --with-registry-auth salesforce-connector
# deploy the JDBC Connector
#docker stack deploy --compose-file connectors/jdbc/docker-compose.yaml --with-registry-auth jdbc-connector
# deploy the BOX Connector
#docker stack deploy --compose-file connectors/box/docker-compose.yaml --with-registry-auth box-connector
# deploy the Workday Connector
#docker stack deploy --compose-file connectors/workday/docker-compose.yaml --with-registry-auth workday-connector
# deploy the Workday Rest Connector
#docker stack deploy --compose-file connectors/workday-rest/docker-compose.yaml --with-registry-auth workday-rest-connector
# deploy the Boomi Connector
#docker stack deploy --compose-file connectors/boomi/docker-compose.yaml --with-registry-auth boomi-connector
# deploy the LastPass Connector
#docker stack deploy --compose-file connectors/lastpass/docker-compose.yaml --with-registry-auth lastpass-connector

# deploy the Thales Connector
#docker stack deploy --compose-file connectors/thales/docker-compose.yaml --with-registry-auth thales-connector
# deploy the Thales WSDL Connector
#docker stack deploy --compose-file connectors/thales-wsdl/docker-compose.yaml --with-registry-auth thales-wsdl-connector
# deploy the PostgreSQL Connector
#docker stack deploy --compose-file connectors/postgresql/docker-compose.yaml --with-registry-auth postgresql-connector
#source adapter
# deploy the Http source adapter
#docker stack deploy --compose-file sourceadapter/http-source-adapter/docker-compose.yaml --with-registry-auth http-source-adapter
# deploy the Kronos Connector
#docker stack deploy --compose-file connectors/kronos/docker-compose.yaml --with-registry-auth kronos-connector
# deploy the Rexx Connector
#docker stack deploy --compose-file connectors/rexx/docker-compose.yaml --with-registry-auth rexx-connector

# deploy reverse proxy
if [ "${OPENIAM_RPROXY_HTTP}" -eq 1 ]; then
docker stack deploy --compose-file rproxy/docker-compose.yaml --with-registry-auth rproxy
else
docker stack deploy --compose-file rproxy-ssl/docker-compose.yaml --with-registry-auth rproxy
fi

CN=$(docker ps | grep cassandra | awk '{print $1}')
docker exec -it $CN bash -c "echo 'alter table janusgraph.edgestore with GC_GRACE_SECONDS = 86400;' > /tmp/file.cql"
docker exec -it $CN cqlsh -u cassandra -p $CASSANDRA_PASSWORD --file /tmp/file.cql