#!/usr/bin/env bash

set -x
set -e

./shutdown.sh

sleep 5

if [[ "$(docker volume ls | grep openiam-elasticsearch-storage_storage)" ]]; then
	docker volume rm openiam-elasticsearch-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-elasticsearch-storage)" ]]; then
	docker volume rm openiam-elasticsearch-storage
	sleep 5
fi

if [[ "$(docker volume ls | grep openiam-postgres-storage_storage)" ]]; then
	docker volume rm openiam-postgres-storage_storage
	sleep 5
fi

if [[ "$(docker volume ls | grep openiam-mysql-storage_storage)" ]]; then
	docker volume rm openiam-mysql-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-mysql-storage)" ]]; then
	docker volume rm openiam-mysql-storage
	sleep 5
fi

if [[ "$(docker volume ls | grep openiam-activiti-storage_storage)" ]]; then
	docker volume rm openiam-activiti-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-activiti-storage)" ]]; then
	docker volume rm openiam-activiti-storage
	sleep 5
fi
if [[ "$(docker volume ls | grep connector_data_storage)" ]]; then
	docker volume rm connector_data_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-rabbitmq-storage_storage)" ]]; then
	docker volume rm openiam-rabbitmq-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-rabbitmq-storage)" ]]; then
	docker volume rm openiam-rabbitmq-storage
	sleep 5
fi
if [[ "$(docker volume ls | grep connector_data_storage_storage)" ]]; then
	docker volume rm connector_data_storage_storage
	sleep 5
fi
if [[ "$(docker network ls | grep openiam_private)" ]]; then
	docker network rm openiam_private
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-janusgraph-storage_storage)" ]]; then
	docker volume rm openiam-janusgraph-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-cassandra-storage_storage)" ]]; then
	docker volume rm openiam-cassandra-storage_storage
	sleep 5
fi
if [[ "$(docker volume ls | grep openiam-janusgraph-storage)" ]]; then
	docker volume rm openiam-janusgraph-storage
	sleep 5
fi
