# Upgrade Instructions

This document describes how to upgrade from one version to another.


## 4.2.0

If upgrading from a pre-4.2.0 version, you will have to do the following:

### Elasticsearch
We've upgraded from elasticsearch 2.x to 6.x (at least).  The data format is quite differnet, so a rolling update is not possible.  Thus, you must do the following

Our elasticsearch container's default UUID/GUID has changed from 100:101 to 1000:1000. You will have to do this

To fix permissions errors associated with this change, please do this

```
chown -R 1000:1000 /var/lib/docker/volumes/openiam-elasticsearch-storage_storage/_data
```

In addition, you will have to run this:

```
cd /var/lib/docker/volumes/openiam-elasticsearch-storage_storage/_data/
mkdir -p data
mv * data/
```

### RabbitMQ

Quite a few of our RabbitMQ declarations have changed.  You will need to delete the rabbitmq volume before continuing.

```
docker volume rm openiam-rabbitmq-storage_storage
```


### Vault

In V4.2.0, we store secrets in Vault.  You will need to set the jks password environment variables when running the vault-bootstrap docker image.
This will ensure that the old keystore will continue to function

To do this, simply do *not* change these values in env.sh:

```
export IAM_JKS_PASSWORD=openiamKeyStorePassword
export IAM_JKS_KEY_PASSWORD=openiamMasterKey
export IAM_JKS_COOKIE_KEY_PASSWORD=openiamCookieKey
export IAM_JKS_COMMON_KEY_PASSWORD=openiamCommonKey
```


### Flyway

In V4.2.0, we introduced Flyway to manage our database state.  This makes upgrading from one point version to another seamless, withouut having
to manually run database changes.  If you're upgrading from a pre-4.2.0, please set this property in env.sh.

Set this value to the next point version.  For example, if you are upgrading from 4.1.5, set this value to 4.1.5.1.  If upgrading from 4.1.5.1,
set this to 4.1.5.2.  If you are upgrading from 4.1.6, set this to 4.1.6.1

```
export FLYWAY_BASELINE_VERSION=<SEE_ABOVE>
```

### Mariadb
Mariadb's defualt location for storing data has changed.  To fix this, please do this:

```
cd /var/lib/docker/volumes/openiam-mysql-storage_storage/_data/
mkdir -p data
mv * data/ # you'll get a warning here, ignore it
```

After running this, consider removing the `openiam` and `activiti` directories


## 4.2.1.3

The `provisionrequest` elasticsearch document can grow to be many GB in size, and thus we must add the possiblity to curate it.

Before deploying and upgrading, you will need to run:
```
curl -XDELETE "http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/provisionrequest"
curl -XDELETE "http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/connectorreply"
curl -XDELETE "http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/provisionconnectorrequest"
```

This can be run on any container with `curl` access, such as the `esb` container

Then, do the following:

1) `git checkout RELEASE-4.2.1.3`
2) `git pull`
3) Resolve any merge conflicts
4) Deploy using `./setup.sh && ./startup.sh`.
5) The flyway container should fail:  `docker ps -a | grep flyway` with a non-zero exit `
6) Modify `utilities/flyway/docker-compose.yaml`, and uncomment this line:
```
      FLYWAY_COMMAND: "repair"
```
7) Deploy using `./setup.sh && ./startup.sh`.
8) Modify `utilities/flyway/docker-compose.yaml`, and comment out this line:
```
#      FLYWAY_COMMAND: "repair"
```
9) Deploy using `./setup.sh && ./startup.sh`.
