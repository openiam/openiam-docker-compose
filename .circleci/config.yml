---
version: 2

jobs:
  build:
    machine:
      image: ubuntu-2204:2023.10.1
    resource_class: xlarge
    environment:
      TZ: "/usr/share/zoneinfo/Etc/UTC"
      DEBIAN_FRONTEND: "noninteractive"
      TERM: "dumb"
      VAULT_JKS_PASSWORD: passwd00
      OPENIAM_VERSION_NUMBER: "4.2.1.12"
    steps:
      - checkout
      - run:
          name: Build 3.2 images
          command: |
             . .ci/setup.sh
             docker swarm init
      - run:
          name: Modify env.sh
          command: |
            sed -i 's|export OPENIAM_RPROXY_HTTP=|export OPENIAM_RPROXY_HTTP=1|g' ./env.sh
            sed -i 's|export LOGGING_LEVEL="WARN"|export LOGGING_LEVEL="INFO"|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_DEBUG=0|export OPENIAM_RPROXY_DEBUG=1|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_VERBOSE=0|export OPENIAM_RPROXY_VERBOSE=1|g' ./env.sh
            echo "export VAULT_JKS_PASSWORD=passwd00" >> env.sh
            echo "export FLYWAY_ACTIVITI_PORT=3306" >> env.sh
            echo "export FLYWAY_ACTIVITI_HOST=database" >> env.sh
            echo "export FLYWAY_OPENIAM_PORT=3306" >> env.sh
            echo "export FLYWAY_OPENIAM_HOST=database" >> env.sh
            echo "export OPENIAM_BASH_LOG_LEVEL=trace" >> env.sh
      - run:
          name: Start Containers
          command: |
             sudo systemctl stop ufw
             ./generate.cert.sh
             ./setup.sh
             ./startup.sh
      - run:
          name: Verify
          command: |
             .ci/wait.sh
             .ci/verify.sh
      - run:
          name: Output ESB Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "esb" | awk '{ print $1 }'
             docker ps -a | grep "esb" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             # docker logs $(docker ps -a | grep "esb" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Busienss Rules Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "business-rule-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output IDM Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "idm" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Workflow Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "workflow" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Email Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "email-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Auth Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "auth-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Reconciliation Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "reconciliation" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Synchronization Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "synchronization" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Groovy Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "groovy-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Flyway Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "flyway" | awk '{ print $1 }'
             docker ps -a | grep "flyway" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Database logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "database" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Elasticsearch Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }'
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             #n docker logs $(docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Cassandra Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "cassandra" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Janusgraph Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "janusgraph" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Vault Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "vault" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Curator Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "curator" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Etcd Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "etcd" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Redis Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "redis" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Rabbitmq Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "/rabbitmq" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output docker ps -a
          no_output_timeout: 1m
          command: |
             docker ps -a
          when: always

  build_arm64:
    machine:
      image: ubuntu-2204:2023.07.1
    resource_class: arm.xlarge
    environment:
      TZ: "/usr/share/zoneinfo/Etc/UTC"
      DEBIAN_FRONTEND: "noninteractive"
      TERM: "dumb"
      VAULT_JKS_PASSWORD: passwd00
      OPENIAM_VERSION_NUMBER: "4.2.1.12"
    steps:
      - checkout
      - run:
          name: Build 3.2 images
          command: |
             . .ci/setup.sh
             docker swarm init
      - run:
          name: Modify env.sh
          command: |
            sed -i 's|export OPENIAM_RPROXY_HTTP=|export OPENIAM_RPROXY_HTTP=1|g' ./env.sh
            sed -i 's|export LOGGING_LEVEL="WARN"|export LOGGING_LEVEL="INFO"|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_DEBUG=0|export OPENIAM_RPROXY_DEBUG=1|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_VERBOSE=0|export OPENIAM_RPROXY_VERBOSE=1|g' ./env.sh
            echo "export VAULT_JKS_PASSWORD=passwd00" >> env.sh
            echo "export FLYWAY_ACTIVITI_PORT=3306" >> env.sh
            echo "export FLYWAY_ACTIVITI_HOST=database" >> env.sh
            echo "export FLYWAY_OPENIAM_PORT=3306" >> env.sh
            echo "export FLYWAY_OPENIAM_HOST=database" >> env.sh
            echo "export OPENIAM_BASH_LOG_LEVEL=trace" >> env.sh
      - run:
          name: Start Containers
          command: |
             sudo systemctl stop ufw
             ./generate.cert.sh
             ./setup.sh
             ./startup.sh
      - run:
          name: Verify
          command: |
             .ci/wait.sh
             .ci/verify.sh
      - run:
          name: Output ESB Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "esb" | awk '{ print $1 }'
             docker ps -a | grep "esb" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             # docker logs $(docker ps -a | grep "esb" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Busienss Rules Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "business-rule-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output IDM Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "idm" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Workflow Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "workflow" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Email Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "email-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Auth Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "auth-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Reconciliation Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "reconciliation" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Synchronization Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "synchronization" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Groovy Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "groovy-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Flyway Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "flyway" | awk '{ print $1 }'
             docker ps -a | grep "flyway" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Database logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "database" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Elasticsearch Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }'
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             #n docker logs $(docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Cassandra Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "cassandra" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Janusgraph Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "janusgraph" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Vault Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "vault" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Curator Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "curator" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Etcd Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "etcd" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Redis Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "redis" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Rabbitmq Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "/rabbitmq" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output docker ps -a
          no_output_timeout: 1m
          command: |
             docker ps -a
          when: always

  build_ce:
    machine:
      image: ubuntu-2204:2023.10.1
    resource_class: xlarge
    environment:
      TZ: "/usr/share/zoneinfo/Etc/UTC"
      DEBIAN_FRONTEND: "noninteractive"
      TERM: "dumb"
      CONTAINER_SERVICE_NAMESPACE: "openiam_service_ce"
      VAULT_JKS_PASSWORD: passwd00
      OPENIAM_VERSION_NUMBER: "4.2.1.12"
    steps:
      - checkout
      - run:
          name: Build 3.2 images
          command: |
             . .ci/setup.sh
             docker swarm init
      - run:
          name: Modify env.sh
          command: |
            sed -i 's|export OPENIAM_RPROXY_HTTP=|export OPENIAM_RPROXY_HTTP=1|g' ./env.sh
            sed -i 's|export LOGGING_LEVEL="WARN"|export LOGGING_LEVEL="INFO"|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_DEBUG=0|export OPENIAM_RPROXY_DEBUG=1|g' ./env.sh
            sed -i 's|export OPENIAM_RPROXY_VERBOSE=0|export OPENIAM_RPROXY_VERBOSE=1|g' ./env.sh
            echo "export VAULT_JKS_PASSWORD=passwd00" >> env.sh
            echo "export FLYWAY_ACTIVITI_PORT=3306" >> env.sh
            echo "export FLYWAY_ACTIVITI_HOST=database" >> env.sh
            echo "export FLYWAY_OPENIAM_PORT=3306" >> env.sh
            echo "export FLYWAY_OPENIAM_HOST=database" >> env.sh
            echo "export OPENIAM_BASH_LOG_LEVEL=trace" >> env.sh
      - run:
          name: Start Containers
          command: |
             ./generate.cert.sh
             ./setup.sh
             ./startup.sh
      - run:
          name: Verify
          command: |
             .ci/wait.sh
             .ci/verify.sh
      - run:
          name: Output ESB Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "esb" | awk '{ print $1 }'
             docker ps -a | grep "esb" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             # docker logs $(docker ps -a | grep "esb" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Busienss Rules Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "business-rule-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output IDM Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "idm" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Workflow Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "workflow" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Email Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "email-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Auth Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "auth-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Reconciliation Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "reconciliation" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Synchronization Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "synchronization" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Groovy Manager Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "groovy-manager" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Flyway Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "flyway" | awk '{ print $1 }'
             docker ps -a | grep "flyway" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Database logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "database" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Elasticsearch Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }'
             docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | while read x; do docker logs $x; done;
             #n docker logs $(docker ps -a | grep "elasticsearch" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Cassandra Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "cassandra" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Janusgraph Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "janusgraph" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Vault Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "vault" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Etcd Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "etcd" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output Redis Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker logs $(docker ps -a | grep "redis" | awk '{ print $1 }' | head -n 1) || exit 0
          when: always
      - run:
          name: Output Rabbitmq Docker logs to console, for easier debugging
          no_output_timeout: 1m
          command: |
             docker ps -a | grep "/rabbitmq" | awk '{ print $1 }' | while read x; do docker logs $x; done;
          when: always
      - run:
          name: Output docker ps -a
          no_output_timeout: 1m
          command: |
             docker ps -a
          when: always




workflows:
  version: 2
  build_local:
    jobs:
      - start_build:
         type: approval
      - build:
         context:
            - Dockerhub
         requires:
            - start_build
      - approve_ce:
         type: approval
      - build_ce:
         context:
            - Dockerhub
         requires:
            - approve_ce
      - approve_arm64:
         type: approval
      - build_arm64:
         context:
            - Dockerhub
         requires:
            - approve_arm64
