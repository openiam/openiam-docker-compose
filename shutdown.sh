#!/usr/bin/env bash

set -x
set -e


docker stack rm ui
docker stack rm openiam
docker stack rm sas-manager
docker stack rm redis
docker stack rm rabbitmq
docker stack rm elasticsearch
docker stack rm kibana
docker stack rm cassandra
docker stack rm metricbeat
docker stack rm filebeat
docker stack rm mariadb
docker stack rm database
docker stack rm postgres
docker stack rm google-connector
docker stack rm ldap-connector
docker stack rm linux-connector
docker stack rm aws-connector
docker stack rm script-connector
docker stack rm oracle-connector
docker stack rm oracle-ebs-connector
docker stack rm scim-connector
docker stack rm oracle-idcs-connector
docker stack rm tableau-connector
docker stack rm adp-connector
docker stack rm ipa-connector
docker stack rm salesforce-connector
docker stack rm workday-connector
docker stack rm workday-rest-connector
docker stack rm jdbc-connector
docker stack rm box-connector
docker stack rm kronos-connector
docker stack rm boomi-connector
docker stack rm lastpass-connector
docker stack rm thales-connector
docker stack rm thales-wsdl-connector
docker stack rm postgresql-connector
docker stack rm rexx-connector
docker stack rm rproxy
docker stack rm flyway
docker stack rm vault
docker stack rm vault-bootstrap
docker stack rm etcd
docker stack rm janusgraph
docker stack rm curator

# NEVER remove these volumes!
# unless they are backed up, this will destroy all encryption related
# these two volumes need to be removed.  They will be re-created with the correct files every time ./startup.sh is run
#docker volume rm vault_client_storage
#docker volume rm vault_server_storage
#docker volume rm connector_data_storage
#docker stack rm upload_storage
#docker stack rm openiam-activiti-storage
#docker stack rm openiam-elasticsearch-storage
#docker stack rm openiam-iamscripts-storage
#docker stack rm openiam-jks-storage
#docker stack rm openiam-rabbitmq-storage
#docker stack rm openiam-janusgraph-storage

sleep 5
