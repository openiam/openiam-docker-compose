#!/bin/bash

set -ex

echo "Sleeping for 2 min at start, so that the UI can come up..."

sleep 120

max_try=7
a=0
while [ $a -le $max_try ]
do
    .ci/verify.sh
    exit_code=$?
    if [ $exit_code -eq 0 ]
    then
        echo "All docker containers started"
        exit 0
    else
        echo "Sleeping for 1 min, so that the UI can come up..."
        sleep 60
    fi
    a=`expr $a + 1`
done

echo "Not started after $max_try checks"
exit 1
