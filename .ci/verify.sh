#!/bin/bash

set -ex

echo "check health for /rabbitmq"
/bin/bash /usr/local/bin/check_health.sh "/rabbitmq" 5
echo "check health for mariadb"
/bin/bash /usr/local/bin/check_health.sh database 5
echo "check health for redis"
/bin/bash /usr/local/bin/check_health.sh redis 5
echo "check health for idm"
/bin/bash /usr/local/bin/check_health.sh idm 5
echo "check health for cassandra"
/bin/bash /usr/local/bin/check_health.sh cassandra 5
echo "check health for janusgraph"
/bin/bash /usr/local/bin/check_health.sh janusgraph 5
echo "check health for groovy_manager"
/bin/bash /usr/local/bin/check_health.sh groovy_manager 5
echo "check health for workflow"
/bin/bash /usr/local/bin/check_health.sh workflow 5
echo "check health for esb"
/bin/bash /usr/local/bin/check_health.sh esb 30
echo "check health for Email Manager"
/bin/bash /usr/local/bin/check_health.sh email-manager 5
echo "check health for Business Rules Manager"
/bin/bash /usr/local/bin/check_health.sh business-rule-manager 5
echo "check health for Device Manager"
/bin/bash /usr/local/bin/check_health.sh device-manager 5
echo "check health for UI"
/bin/bash /usr/local/bin/check_health.sh ui:debian 30

echo "check health for LDAP Connector"
/bin/bash /usr/local/bin/check_health.sh ldap-connector 30
