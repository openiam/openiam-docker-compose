#!/bin/bash

set -e

docker login -u ${CONTAINER_REGISTRY_USER} -p ${CONTAINER_REGISTRY_PASS} ${CONTAINER_REGISTRY}
docker pull ${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/circleci_build_utils:${OPENIAM_VERSION_NUMBER}
docker run -dit ${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/circleci_build_utils:${OPENIAM_VERSION_NUMBER}
docker cp $(docker ps -a -q -n 1):/opt/openiam/webapps/ /tmp/
sudo chown circleci /tmp/webapps/scripts/*.sh
sudo cp -r /tmp/webapps/scripts/*.sh /usr/local/bin/
