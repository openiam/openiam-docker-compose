# Deploying OpenIAM to Docker Swarm

The OpenIAM Docker Swarm deployment method is supported for the following versions of Docker Community Edition (CE) and Docker Compose.

OpenIAM use docker-compose file format **3.2** - supported for versions **17.04.0** or higher of Docker Community Edition (CE) and Docker Compose.

This repository consists of several stacks that are deployable to the Docker swarm. These are:


# Warning

Due to limitations in swarm's architecture, this is NOT an HA deployment, and is no longer meant for production use.  Although we maintain this repository, and ensure that it properly works, we *highly* recommend using
our [kuberentes](https://bitbucket.org/openiam/kubernetes-docker-configuration) deployment structure, which is a true HA deployment, enables horizontal scaling across N nodes.


# Stacks

The OpenIAM Docker Compose repository in Docker consists of several stacks that are deployable to the Docker swarm. These are:

## Critical infrastructure stacks

| Stack Name    | Description                                                                                                                                                                                                                                                            |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Elasticsearch | runs Elasticsearch. Elasticsearch is an enterprise-level search engine based on Lucene.  Elasticsearch uses an index-based search approach, which allows executing searches speedily.  The architecture allows for scalability, flexibility, and multitenancy support. |
| Redis         | uns Redis. Redis is the in-memory data structure store used as a database, cache, and message broker by OpenIAM.                                                                                                                                                       |
| MariaDB       | runs MariaDB, if using MariaDB as datasource. MariaDB is an open source database server that uses a relational database and SQL to manage data.                                                                                                                        |
| PostgreSQL    | runs PostgreSQL, if using PostgreSQL as datasource.,PostgreSQL is a powerful, open source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance.      |
| RabbitMQ      | runs RabbitMQ. RabbitMQ is the message brokering software service for sending and receiving messages between systems.                                                                                                                                                  |
| Vault         | runs Hashicorp Vault.  Vault secures, stores, and tightly controls access to tokens, passwords, certificates, API keys, and other secrets                                                                                                                              |
| Etcd          | runs Etcd, which is used to store Vault data.  Etcd is a distributed key-value store.                                                                                                                                                                                  |



## Service stacks

| Stack Name            | Description                                                                                                                                                                                                                                                                                                                     |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OpenIAM core services | runs services shared across the product.                                                                                                                                                                                                                                                                                        |
| Identity manager      | runs the identity manager application. Identity manager automates the task of managing identities across various devices and applications used by the enterprise.                                                                                                                                                               |
| Workflow              | runs the workflow application. A workflow is a repeatable process during which documents, information, or requests are passed from one participant to another for action, according to a set of procedural rules. A participant can be a person, machine, or both.                                                              |
| Groovy script manager | runs Groovy Manager, an application for managing Groovy scripts in OpenIAM. Apache Groovy is a dynamic programming language for the Java platform. allows you to add, update, edit, and modify Groovy scripts to extend the identity governance and web access management functionality to meet specific, complex requirements. |
| Synchronization       | runs the synchronization application. Synchronization allows you to synchronize data from one or more authoritative sources to a set of managed systems. Synchronization configuration enables monitoring a source system for changes and then updating target systems at scheduled periodic intervals.                         |
| Reconciliation        | runs the reconciliation application. This is two side synchronization between Openiam and the target system.                                                                                                                                                                                                                    |
| Authorization Manager | runs the authorization manager.  This module handles RBAC authorization via relationships between Users, Organizations, Roles, Groups, and Resources.                                                                                                                                                                           |
| Email Manager         | runs the email manager.  Handles sending and receiving email.                                                                                                                                                                                                                                                                   |

## UI Stack

Tomcat with Webconsole, IdP, and Self-Service web applications.
Webconsole is the OpenIAM web application for administrators for managing identities across various devices and applications used by an enterprise, and for controlling access to these devices and applications.Self-Service is the OpenIAM end user web application that allows users to create new requests, reset and change passwords, manage their profiles, manage access requests, manage challenge response security questions, look up corporate users through a directory search, and reset their accounts if they are locked out. Authorized users can also use the request approval functionality.


## Reverse  Proxy Stack

| Stack Name | Description                                                                    |
|------------|--------------------------------------------------------------------------------|
| rproxy     | gateway between clients and a server for managing inbound traffic to a server. |



# System Requirements

Docker deployment for OpenIAM is supported for the following 64-bit Linux and OSX distributions

## Hardware system requirements

OpenIAM recommends the following minimum system configuration:

| Production environment         | Virtual machine (VM)                           | Proof of Concept (PoC) VM                      |
|--------------------------------|------------------------------------------------|------------------------------------------------|
| * Quad-core processor          | * Two virtual central processing units (vCPUs) | * Six virtual central processing units (vCPUs) |
| * 12 GB of RAM (system memory) |  * 12 GB of RAM (system memory)                | * 12 GB of RAM (system memory)                 |
| * 25 GB of free disk space     |  * 25 GB of free disk space                    | * 25 GB of free disk space.                    |


## Software system requirements

* Docker - 17.04.0 or higher
* Docker Compose - 17.04.0 or higher

# Downloading

All deployment files required to create an OpenIAM instance are located in the OpenIAM Docker deployment repository on Bitbucket.

To clone the repository, run the following commands:

```
git clone git@bitbucket.org:openiam/openiam-docker-compose.git
git checkout RELEASE-4.2.1.12
```

# Upgrading

Upgrade instructions are located in the [upgrade guide](upgrading.md).  Use this to upgrade from previous versions of OpenIAM


# Overview

The following scripts are included:

| Script          | Description                                                                                                                                                                                                                                                                                                                                                                              |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| env.sh          | File containing environment variables. The required environment variables can be updated and added in this file. The env.sh file is sourced during the installation process and the export statements in this file are executed.                                                                                                                                                         |
| setup.sh/setup_arm.sh| Script for setting up and updating the OpenIAM configuration on amd64/arm64. During the initial OpenIAM deployment, this script initializes the network and pulls the latest images from the OpenIAM repository (openiamdocker) on Docker Hub.  |
|                 | When updating the OpenIAM deployment, running this script pulls newer images from the OpenIAM repository on Docker Hub. |
| startup.sh/startup_arm.sh| Script for starting up the OpenIAM instance on amd64/arm64.When updating the OpenIAM deployment, running this script updates the configuration on your system with the latest release updates.|
|                 | **Warning:  Please do not modify this script in any way.**                                                    |
| shutdown.sh     | Script for shutting down all OpenIAM stacks, except volumes.                                                                                                                                                                                                                                                                                                                             |
| teardown.sh     | Script for tearing down all OpenIAM stacks, volumes, and networks.                                                                                                                                                                                                                                                                                                                       |
| genrate.cert.sh | Script to generate certificates or Vault authentication.   


The following YAML configuration files are included for providing configuration settings for the OpenIAM instance.

| Folder name    | Configuration settings (YAML) file name             |
|----------------|-----------------------------------------------------|
| connectors     | * Google connector - google/docker-compose.yaml     |
|                | * LDAP connector - ldap/docker-compose.yaml         |
| infrastructure | * Elasticsearch - elasticsearch/docker-compose.yaml |
|                | * MariaDB - mariadb/docker-compose.yaml             |
|                | * RabbitMQ - rabbitmq/docker-compose.yaml           |
|                | * Redis - redis/docker-compose.yaml                 |
| metadata       | docker-compose.yaml                                                                                                                                                                                         |
| rproxy         | docker-compose.yaml                                                                                                                                                                                         |
| rproxy-ssl     | docker-compose.yaml                                                                                                                                                                                         |
| services       | docker-compose.yaml                                                                                                                                                                                         |
| ui             | docker-compose.yaml                                                                                                                                                                                         |


# Preparing your system

Prepare your system for deploying an OpenIAM instance by performing the procedures described in this section in the order that they are presented.

## Configuring persistent settings for Elasticsearch

OpenIAM uses Elasticsearch as a search engine. Elasticsearch uses *mmap*, a Unix system call that maps files or devices into memory, to map portions of an index into the Elasticsearch address space for fast access. To use *mmap* effectively, Elasticsearch requires sufficient *mmap* counts. The default operating system limits on *mmap* counts are inadequate for the required performance and this may result in out of memory exceptions. The required *mmap* value can be configured using the `vm.max_map_count` setting via sysctl to be at least 262144. To ensure that the vm.max_map_count persists across restarts, set this value in the /etc/sysctl.conf file. For more information, see [Bootstrap Checks](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/bootstrap-checks.html) in [Elasticsearch Reference](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/index.html).

To ensure that the docker host(s) have the required vm.max_map_count that persists across restarts, perform the following steps:

1) Open the `/etc/sysctl.conf` file for editing.
2) Add the following line in the sysctl.conf file: `vm.max_map_count=262144`

Save and close the file.
To verify the value is set correctly, reboot the system and run `sysctl vm.max_map_count` in a terminal.

## Disabling IPv6 on Docker host

By default, IPv6 is disabled in Docker. Disabling IPv6 on Docker host(s) prevents any potential network issues. To disable IPv6 on host(s) where Docker is running, ensure that the Docker host(s) have the following value set in **/etc/sysctl.conf**:

```
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
```

You can run `sudo sysctl -p` to apply this setting without restarting the system.

For more information, see [IPv6 with Docker](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/ipv6/).

## Installing Docker Community Edition (CE)

This deployment is supported for Docker Community Edition (CE) version **17.04.0** or higher

Perform the steps described in the Docker documentation links below for installing Docker CE:

* [Install Docker CE for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
* [Install Docker CE for Debian](https://docs.docker.com/install/linux/docker-ce/debian/#install-docker-ce)
* [Install Docker CE for Ubuntu and Linux Mint](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

## Installing Docker Compose

Please use the [following guide](https://docs.docker.com/compose/install/) for installing Docker Compose

## Setting environment variables

The environment variables described here must be verified or defined before running the deployment scripts. These environment variables are set in the **env.sh** script file in the Docker Compose repository folder. The **env.sh** file is sourced during the installation process and the export statements in this file are executed.

<div style="border: 1px solid #ff3333; padding-right: 5px; padding-left: 5px; padding-top: 10px; padding-bottom: 10px; margin-top: 15px; margin-bottom: 15px;"><span style="color: #ff3333; font-weight: bold">Warning:</span> Please do not modify the **startup.sh** script.</div>

The table below describes the environment variables that are required to be set.

| Environment variable          | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `LOGGING_LEVEL`               | <span>The logging level of OpenIAM modules. The values that can be specified are:</span><ul><li>ERROR - shows messages about errors that prevent the application from running correctly and require intervention for fixing.</li><li>WARN - shows warning messages. These warnings are about potentially harmful situations.</li><li>INFO - informational messages about the progress of the application.</li><li>DEBUG - fine-grained informational messages that are useful for debugging the application.</li><li>TRACE - finer-grained informational messages than `DEBUG` that can be used to find one part of a function specifically.</li></ul>In production environments, it is recommended that you specify the logging level as either `WARN` or `ERROR`. If you set the logging level as `DEBUG`, a lot of information is generated, which can lead to system overload and disk space issues. |
| `OPENIAM_VERSION_NUMBER`      | The OpenIAM version that you wish to run. For OpenIAM 4.2.0, set this environment variable to `4.2.0`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `BUILD_ENVIRONMENT`           | The OpenIAM environment that you are pulling from Docker. Valid values are `latest`, `dev`, `qa`, and `prod`. By default, the build environment is set to `prod`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `REDIS_PASSWORD`              | The password that is used to communicate with Redis, the in-memory data structure store that is used as a database, cache, and message broker.<br /><br />By default, it is set as `export REDIS_PASSWORD="passwd00"`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `RABBITMQ_PASSWORD`        | This is the password that will be used to communicate with RabbitMQ.<br /><br />By default, it is set as `export RABBITMQ_PASSWORD="passwd00"`.  
                                     |
| `DB_ROOT_PASSWORD`         | The root password that is used to set up MySQL.<br /><br /><div style="border:1px solid #169998; margin-top: 15px; margin-bottom: 15px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;"><span style="color: #169998; font-weight: bold;">Note:</span>This password stays internal to the MySQL Docker container to ensure security.</div><br /><br />By default, it is set as export MYSQL_ROOT_PASSWORD="passwd00".                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `DB_TYPE`                     | The database type to be used. Valid values are `MariaDB`, `Posgres`, `MSSQL`, and `Oracle`. By default, the database type is set to `MariaDB`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `LDAP_KEYSTORE_PASSWORD`      | Password of your keystore. The default value is `changeit`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `OPENIAM_DEFAULT_URI`         | The URL to which the application defaults when a user enters an unknown URL. By default, the URL is specified as `/selfservice/`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `SYS_LOG_HOST`                | The Docker host server. This environment variable, along with the `DOCKER_HOST_IP`, allows redirecting syslogs from OpenIAM to the Docker host server.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `DOCKER_HOST_IP`              | The IP address of the Docker host. This environment variable, along with the `SYS_LOG_HOST`, allows redirecting syslogs from OpenIAM to the Docker host server.<br /><br />Use the OS command to get the IP address or set it manually. For example `export DOCKER_HOST_IP=172.18.0.1` or <code>export DOCKER_HOST_IP=$(ifconfig docker_gwbridge |awk '/inet/ {print $2}')</code> (example for CentOS 7 OS). The default value (`127.0.0.1`) does not redirect logs to the Docker host.                                                                                                                                                                                                                                                                                                                                                                      |
| `REDISSON_TIMEOUT`            | The server response timeout in milliseconds. The default value for the response timeout is 5000 (set by `export REDISSON_TIMEOUT="5000"` in the file).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `JDBC_OPENIAM_DB_USER`        | This is the username to the openiam database |
| `JDBC_OPENIAM_DB_PASSWORD`    | This is the password to the opeiam database |
| `JDBC_ACTIVITI_DB_USER`       | This is the username to the activiti database |
| `JDBC_ACTIVITI_DB_PASSWORD`   | This is the password to the activiti database |
| `OPENIAM_DATABASE_NAME`       | The openiam database name |
| `ACTIVITI_DATABASE_NAME`       | The openiam database name |
| `KEYSTORE_PSWD`               | This is the java keystore password |
| `IAM_JKS_PASSWORD`            | The password encyrption key master password |
| `IAM_JKS_KEY_PASSWORD`        | The password encryption key password |
| `IAM_JKS_COOKIE_KEY_PASSWORD` | The cookie key password |
| `IAM_JKS_COMMON_KEY_PASSWORD` | The common key password |
| `VAULT_JKS_PASSWORD`          | The password of the Vault Certificate.  See the [Vault](#Vault) section for information about setting up the Vault Certificate |

# Vault

OpenIAM uses Vault in order to store secrets, such as database passwords, redis passwords, etc.  Communication with Vault occurs via a certificate.

## Generate CA Certiiacte

First, you should generate a CA Certiciate.  If you have an existing CA Certificate from a trusted CA, then you should use that.
Otherwise, you can use the below commands.

```
./generate.cert.sh
```

Answer all questions.  Make sure to use `vault` as the Common Name


# Reverse Proxy

## Configure HTTP or HTTPS in rproxy

To configure HTTP or HTTPS in reverse proxy (rproxy), set environment variables in the **env.sh** script file. The **env.sh** script file is included in the Docker Compose folder for deploying OpenIAM.

You can set the `OPENIAM_RPROXY_HTTP` environment variable to `1` to disable HTTPS. If you set the `OPENIAM_RPROXY_HTTP` environment variable to `0`, rproxy redirects all requests from HTTP to HTTPS.


If you need to configure HTTPS, you will need to set the correct file names for `OPENIAM_SSL_CERT_PATH` and `OPENIAM_SSL_KEY_PATH` environment variables for certificates. The files with certificates and keys should be in `/opt/openiam/httpd/ssl-certs` and `/opt/openiam/httpd/ssl-keys`.

The rproxy Docker container is running as user 'rproxy' with uid = 75001 in the Docker container. As the rproxy Docker container is not running as 'root', it has no access by default to the `/opt/openiam/httpd` directory. To configure access to it, the user 'rproxy' with uid = 75001 should be created on the host machine and the `/opt/openiam/httpd` directory should be configured to have read access for user 'rproxy'. The rproxy Docker container can then map directories from `/opt/openiam/httpd` to Docker volumes and use certificates and keys stored there.

You can use the following command to create user 'rproxy' with uid=75001:

```
sudo groupadd -g 75001 rproxy
sudo useradd --shell /sbin/nologin -u 75001 -g 75001 rproxy
```

The following is an example of how to make user 'rproxy' the owner of the `/opt/openiam/httpd` directory and set up access:

```
sudo chown -R rproxy /opt/openiam/httpd
sudo chmod -R 755 /opt/openiam/httpd/ssl-certs
sudo chmod -R 700 /opt/openiam/httpd/ssl-keys
```

In a multi-node environment, copy these files to other Docker nodes in the same directories too. Or, configure the `/opt/openiam/httpd` directory so that it can be accessed from multiple nodes.

You must to set CA certificate or CertificateChain with CA certificates. For this, you can use `OPENIAM_SSL_CHAIN_PATH` or `OPENIAM_SSL_CA_PATH` environment variable. These variables copy files from the specified paths to `/opt/openiam/httpd/ssl-certs` and set [SSLCACertificateFile](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslcacertificatefile) or [SSLCACertificateFile](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslcacertificatefile) in Apache configurations. These files should also be accessible from other Docker nodes at this path `opt/openiam/httpd/ssl-certs`.

For configuring HTTPS, set the following environment variables in the env.sh script file:

| Environment variable          | Description |
|------------------------|--------------|
| `$OPENIAM_RPROXY_HTTP` | To use HTTPS, set this environment variable as follows:<br/><br/>`OPENIAM_RPROXY_HTTP=0`<br/></br/>If you wish to use HTTP instead of HTTPS, set this environment variable as follows:<br/></br/>`OPENIAM_RPROXY_HTTP=1.`<br/><br/><div style="border:1px solid #169998; margin-top: 15px; margin-bottom: 15px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;"><span style="color: #169998; font-weight: bold;">Note:</span>If you have a load balancer or another reverse proxy in front of the rproxy dockerimage that has HTTPS configured, you can set `OPENIAM_RPROXY_HTTP=1` and all variables named `OPENIAM_SSL_` will be ignored.</div>|
| `OPENIAM_SSL_CERT`     | Path to the certificate file in PEM format for Apache. This should be in `/opt/openiam/httpd/ssl-certs`. In a multi-node environment, copy this to the directory on other Docker nodes too.|
| `OPENIAM_SSL_CERT_KEY` | Path to the key for the certificate file for Apache. This should be in `/opt/openiam/httpd/ssl-keys`. In a multi-node environment, copy this to the directory on other Docker nodes too. |
| `OPENIAM_SSL_CHAIN`    | Path of the file to use as certificate authority chain file. Used in [SSLCertificateChainFile](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslcertificatechainfile) in Apache configurations. This should be in `/opt/openiam/httpd/ssl-certs`. In a multi-node environment, copy this to the directory on other Docker nodes too.|
| `OPENIAM_SSL_CA`       | Path of the directory of PEM CA certificate. Used in [SSLCACertificateFile](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslcacertificatefile) in Apache configurations. This should be in `/opt/openiam/httpd/ssl-certs`. In a multi-node environment, copy this to the directory on other Docker nodes too. |

You must set `OPENIAM_RPROXY_HTTP` before you run the **setup.sh** script. If you run the **setup.sh** script without setting `OPENIAM_RPROXY_HTTP`, the following message is displayed:

```
set OPENIAM_RPROXY_HTTP to 1 (recommended) or 0 (not recommended, unless you have load balancer with https in front of rproxy)
```

A similar message is displayed if `OPENIAM_RPROXY_HTTP` is set to `0`, but no certificates are set in `OPENIAM_SSL_CERT` and `OPENIAM_SSL_CERT_KEY`.

## Optional rproxy environment variables

This section describes optional reverse proxy (rproxy) environment variables that you can set as per your requirement.

### SSL Options

#### Optional rproxy environment variables

Docker containers (images) are immutable.The optional rproxy environment variables described here allow changing configuration in the rproxy Docker container. By default, Apache uses the following SSL protocols and ciphers options

```
SSLProtocol all -SSLv2 -SSLv3
SSLCipherSuite HIGH:3DES:!aNULL:!MD5:!SEED:!IDEA
```

It is possible to change these settings by specifying the following environment variables in the env.sh script file:

| Environment variable          | Description |
| `OPENIAM_SSL_PROTOCOL` | SSL protocol version (for example, SSLv2 and SSLv3). Value from this environment variable is used in the [SSLProtocol](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslprotocol) Apache configuration option.|
| `OPENIAM_SSL_CIPHER_SUITE` | Cipher suite available for negotiation in SSL handshake. The value from this environment variable is used in the [SSLCipherSuite](https://httpd.apache.org/docs/2.4/mod/mod_ssl.html#sslciphersuite) Apache configuration option. |

For example, to disable everything except TLSv1.2 and allow only ciphers with high encryption, set the values as follows in the **env.sh** script file:

```
export OPENIAM_SSL_PROTOCOL="-ALL -TLSv1 -TLSv1.1 +TLSv1.2"
export OPENIAM_SSL_CIPHER_SUITE="HIGH:!MEDIUM:!aNULL:!MD5:!RC4"
```

### Configure HTTP and HTTPS on non-default ports

By default, if `OPENIAM_RPROXY_HTTP` is set to `0`, HTTPS is configured on default port `443`, HTTP on port `80` just does redirects from HTTP to HTTPS. To configure HTTPS on a non-default port, you need to modify `/rproxy/docker-compose.yaml` or `/rproxy-ssl/docker-compose.yaml` configuration (in the 3.2/rproxy and 3.2/rproxy-ssl folder in the OpenIAM Docker repository) and set needed port in ports section.

The default configuration in docker-compose.yaml is as follows:

```
networks:
      - openiam_private
    ports:
      - "80:80"
      - "443:443"
    deploy:
```

The following is HTTP on port 8000 and HTTPS on port 8001:
```
networks:
      - openiam_private
    ports:
      - "8000:80"
      - "8001:443"
    deploy:
```

Important:  Do not forget to add firewall rules on the host to allow traffic to this non-default ports.

With HTTPS working on a non-default port, it is important to modify redirect from HTTP to HTTPS. To do this, specify the `OPENIAM_HTTPS_HOST` environment variable. Specify the full address with scheme and port, as shown in the example below:

```
export OPENIAM_HTTPS_HOST=https://node1.openiam.com:8001
```

By default apache mod_deflate compression ratio set to 6
Values can be set from 0 to 9. 0 - No compression, 9 - Maximum compression

```
export OPENIAM_RPROXY_DEFLATE=6
```

By default, ServerName not set in apache configs, this can produce warning like that:
`httpd: Could not reliably determine the server's fully qualified domain name`
ServerName in apache configs can be set using this environment variable:

```
export OPENIAM_RPROXY_SERVER_NAME=www.example.com
```



### Debugging rproxy

You can set the following optional environment variables in the **env.sh** script file for debugging the rproxy Docker container.

| Environment variable          | Description  |
|-------------------------------|--------------|
| `OPENIAM_RPROXY_DEBUG` | If set to `1`, changes the [LogLevel](https://httpd.apache.org/docs/2.4/mod/core.html#loglevel) warn (warning conditions) in Apache configuration to LogLevel debug (debug-level messages). |
| `OPENIAM_RPROXY_VERBOSE` | If set to `1`, sets rproxy to produce more verbose logging. |
| `OPENIAM_RPROXY_DEBUG_ESB` | If set to `1`, enables rproxy to log communications with OpenIAM ESB services. Can be used for debugging and troubleshooting. |
| `OPENIAM_RPROXY_DEBUG_AUTH` | If set to `1`, enables rproxy to log more debug information for authentication. Can be used for debugging and troubleshooting. |
| `OPENIAM_RPROXY_DUMP_ALL` | Please use this option with caution, because it print all requests information, including headers, cookies and bodies in logs. This is insecure and should be used temporary for debug purposes only. This also can produce a lot of logs |


### Loggging rproxy

Reverse proxy logging, by default, redirect error log to /dev/stderr and access log to /dev/stdout.
You can change that using OPENIAM_RPROXY_ERROR_LOG and OPENIAM_RPROXY_ACCESS_LOG environment variables
For example use /dev/stdout for both or set access log to /dev/null and log only errors.

```
export OPENIAM_RPROXY_ERROR_LOG=/dev/stderr
export OPENIAM_RPROXY_ACCESS_LOG=/dev/stdout
```

### Changing OpenIAM ESB and UI addresses in rproxy

If the rproxy Docker container is running in a swarm, the OpenIAM core services can be accessed using the **http://esb:9080** address and OpenIAM UI is reachable at **http://ui:8080**. However, if you run rproxy as a standalone Docker container, you need to use different addresses for OpenIAM ESB and OpenIAM UI. It is possible to change these addresses using the following optional environment variables:

| Environment variable          | Description  |
|-------------------------------|--------------|
| `OPENIAM_RPROXY_ESB` | OpenIAM core services address, for example, `http://localhost:9080`. |
| `OPENIAM_RPROXY_UI` | OpenIAM UI address, for example, `http://localhost:8080`. |



### Add default redirect

If no URI specified when accessing the OpenIAM UI, it is possible to redirect the user to a specific URI. For example, if a user is attempting to access `http://demo.openiamdemo.com/`, then, instead of showing a 404 error (not found), it is possible to redirect the user to a default `/selfservice` URI: `http://demo.openiamdemo.com/selfservice/`.

This default is set using the `OPENIAM_DEFAULT_URI` environment variable in the **env.sh** script file in the Docker Compose repository folder (`OPENIAM_DEFAULT_URI=/selfservice/`).

##### Content-Security-Policy and Cross-Origin Resource Sharing

Content-Security-Policy headers enabled by default. To disable set to '0'
```
export OPENIAM_RPROXY_CSP=1
```

Cross-Origin Resource Sharing by default set to 1. To disable set to '0'
```
export OPENIAM_RPROXY_CORS=1
```

### Apache extra configs

The following optional rproxy environment variables in the **env.sh** script file allow adding additional configuration to Apache running in the rproxy Docker container.

| Environment variable          | Description  |
|-------------------------------|--------------|
| `OPENIAM_APACHE_EXTRA_CONF` | File that is included in **httpd.conf** in the rproxy Docker container.<br/><br/>The file specified by `OPENIAM_APACHE_EXTRA_CONF` should be in **/opt/openiam/httpd/conf-add** directory and is included in **httpd.conf** in the rproxy Docker container.<br/><br/>For example, to disable the output of Apache version, use openiam_httpd_extra.conf with this content:`````` |
|                             | ```# openiam_httpd_extra.conf |
|                             |     # Do not output apache version information |
|                             |     ServerTokens Prod |
|                             |     ServerSignature Off``` |
|                             | Put the file namein `OPENIAM_APACHE_EXTRA_CONF`, as shown below: |
|                             | ```export OPENIAM_APACHE_EXTRA_CONF=openiam_httpd_extra.conf``` |
| `OPENIAM_VHOST_EXTRA_CONF` | This file will be included inside VirtualHost section in apache configs in rproxy docker container |



# Database

## Flyway

Starting in V4.2.0, we use Flyway to manage database migrations.  This ensure that your database is propertly versioned and up-to-date.
We support Flyway versioning with Mariadb, Postgresql, and MSSQL, and Oracle 12.2+

To enable Flyway, set the following properties in **env.sh**

```
export FLYWAY_ENABLED=1
```


If this is a new install, set the `FLYWAY_BASELINE_VERSION` to `2.3.0.0`
```
export FLYWAY_BASELINE_VERSION=2.3.0.0
```

If you are upgrading from pre-4.2.0, set `FLYWAY_BASELINE_VERSION` to the next point.  For example, if you are upgrading from `4.1.5`, set this value to `4.1.5.1`. If upgrading from `4.1.5.1`, set this to `4.1.5.2`.  If you are upgrading from `4.1.6`, set this to `4.1.6.1`.
Don't worry if this `next` point version does not exist - this is simply a placeholder for flyway to know when to start

```
export FLYWAY_BASELINE_VERSION=4.1.6.1
```

Set the name of the Activiti Database.  We default to the value of `ACTIVITI_DATABASE_NAME`, which should resolve most use-cases
```
# name of the activiti database.  If using mariadb, this is likely 'activiti'
export FLYWAY_ACTIVITI_DATABASE_NAME=$ACTIVITI_DATABASE_NAME
```

Set the name of the Openiam Database.  We default to the value of `OPENIAM_DATABASE_NAME`, which should resolve most use-cases
```
# name of the openiam core database.  If using mariadb, this is likely 'openiam'
export FLYWAY_OPENIAM_DATABASE_NAME=$OPENIAM_DATABASE_NAME
```

```
# port of the activiti database.  If using mariadb, this is likely '3306'.  If using postgres, this is likely '5432'
export FLYWAY_ACTIVITI_PORT=3306
```

```
# host of the activiti database.  If using mariadb or postgres in docker, this is likely 'database'
export FLYWAY_ACTIVITI_HOST=mariadb
```

```
# port of the openiam database.  If using mariadb, this is likely '3306'.  If using postgres, this is likely '5432'
export FLYWAY_OPENIAM_PORT=3306
```

```
# host of the openiam database.  If using mariadb or postgres in docker, this is likely 'database'
export FLYWAY_OPENIAM_HOST=mariadb
```

Set the name of the Activiti User.  We default to the value of `JDBC_ACTIVITI_DB_USER`, which should resolve most use-cases
```
# username to activiti database
export FLYWAY_ACTIVITI_USERNAME=$JDBC_ACTIVITI_DB_USER
```

Set the name of the Activiti Password.  We default to the value of `JDBC_ACTIVITI_DB_PASSWORD`, which should resolve most use-cases
```
# password to activiti database
export FLYWAY_ACTIVITI_PASSWORD=$JDBC_ACTIVITI_DB_PASSWORD
```

Set the name of the Openiam User.  We default to the value of `JDBC_OPENIAM_DB_USER`, which should resolve most use-cases
```
# username to openiam database
export FLYWAY_OPENIAM_USERNAME=$JDBC_OPENIAM_DB_USER
```

Set the name of the Openiam Password.  We default to the value of `JDBC_OPENIAM_DB_PASSWORD`, which should resolve most use-cases
```
# password to openiam database
export FLYWAY_OPENIAM_PASSWORD=$JDBC_OPENIAM_DB_PASSWORD
```

**Warning**.  In Postgres, the user executing the flyway scripts against Postgres *must* be a superuser.

#### Post Setup Scripts

If you need perform custom SQL scripts on the 1st service run, or when upgrading
 please mount a volume into flyway with the proper naming convention.

For example, to run custom scripts after the 4.2.0 migration, add the following volume to the *flyway* container:

```
    volumes:
    - path_on_host:/data/openiam/conf/schema/mysql/openiam/4.2.0.0/custom/
```

Scripts in this folder are expected to be of the following format:
```
V4.2.0.0.9XX__description.sql
```

Where X is a number from 0-9.  Scripts will be run *in order*

Below is another naming example, when setting up 4.2.1

```
    volumes:
    - path_on_host:/data/openiam/conf/schema/mysql/openiam/4.2.1.0/custom/
```

Scripts in this folder are expected to be of the following format:
```
V4.2.1.0.9XX__description.sql
```

Below is another naming example, when setting up a 4.2.1.1 post setup scripts.

```
    volumes:
    - path_on_host:/data/openiam/conf/schema/mysql/openiam/4.2.1.1/custom/
```

Scripts in this folder are expected to be of the following format:
```
V4.2.1.1.9XX__description.sql
```


All scripts from there will be collected and performed after DB installation


## Supported Databases

#### MariaDB

Make sure to set the DB_TYPE environment variable to `MariaDB` in setup.sh

```
export DB_TYPE="MariaDB"
```


#### SQLServer

If you are using MSSQL, you have to tell the ESB and Workflow that you are using that database.  To do this,
include the following environment variables in the services docker-compose.yaml descriptor.

Change the Hibernate Dialect Accordingly to your SQLServer version.
```
      JDBC_HOST: "${JDBC_HOST}" # the hostname of the database
      JDBC_PORT: "${JDBC_PORT}" # port of the database
      JDBC_SCHEMA_NAME: "${JDBC_SCHEMA_NAME}" # openiam database schema (i.e. 'openiam.dbo')
      ACTIVITI_SCHEMA_NAME: "${ACTIVITI_SCHEMA_NAME}" # openiam database schema (i.e. 'activiti.dbo')
      JDBC_DATABASE_NAME: "${OPENIAM_DATABASE_NAME}" # name of the openiam database (i.e. 'openiam')
      JDBC_ACTIVITI_DATABASE_NAME: "${ACTIVITI_DATABASE_NAME}" # name of the activiti database (i.e. 'activiti')
      OPENIAM_HIBERNATE_DIALECT: "org.hibernate.dialect.SQLServer2012Dialect" # hibernate dialect for your SQLServer Version
```

Make sure to set the DB_TYPE environment variable to `MSSQL` in setup.sh

```
export DB_TYPE="MSSQL"
```


#### PostgreSQL

Make sure to set the DB_TYPE environment variable to `Postgres` in setup.sh

```
export DB_TYPE="Postgres"
```

#### Oracle

If you are using Oracle, you have to tell the ESB and Workflow that you are using that database.  To do this,
include the following environment variables in the services docker-compose.yaml descriptor.

Change the Hibernate Dialect Accordingly to your Oracle version
```
      JDBC_HOST: "${JDBC_HOST}" # the hostname of the database
      JDBC_PORT: "${JDBC_PORT}" # port of the database
      JDBC_SID: "${JDBC_SID}" # SID for connecting to your Oracle database
      JDBC_SERVICE_NAME: "${JDBC_SERVICE_NAME}" # Service Name for connecting to your Oracle database
      JDBC_SCHEMA_NAME: "${JDBC_SCHEMA_NAME}" # openiam database schema (i.e. 'IAMUSER')
      ACTIVITI_SCHEMA_NAME: "${ACTIVITI_SCHEMA_NAME}" # openiam database schema (i.e. 'ACTIVITI')
      JDBC_DATABASE_NAME: "${OPENIAM_DATABASE_NAME}" # name of the openiam database (i.e. 'IAMUSER')
      JDBC_ACTIVITI_DATABASE_NAME: "${ACTIVITI_DATABASE_NAME}" # name of the activiti database (i.e. 'ACTIVITI')
      OPENIAM_PROP_user_timezone: "${ORACLE_USER_TIMEZONE}" # property required by Oracle
      OPENIAM_HIBERNATE_DIALECT: "org.hibernate.dialect.Oracle10gDialect" # hibernate dialect for your Oracle Version
```

If you are using the Oracle SID to connect to Oracle, set the `JDBC_SID` variable.
If you are using the Oracle Service Name to connect to Oralce, set the `JDBC_SERVICE_NAME` variable

Make sure to set the DB_TYPE environment variable to `Oracle` in setup.sh

```
export DB_TYPE="Oracle"
```

## Swarm and Disk Driver Config

By default, the OpenIAM shell scripts deploy to the Docker swarm. Ensure that the necessary ports are opened. Otherwise, the manager and worker node(s) will not be able to communicate with each other. For more information, see [Getting started with swarm mode](https://docs.docker.com/engine/swarm/swarm-tutorial/).

The following environment variables must be verified or defined for configuring the Docker swarm and the disk driver setting. These environment variables are set in the env.sh script file.


| Environment variable          | Description  |
|-------------------------------|--------------|
| `NODE_ROLE`                   | the Node Role can either be `worker` or `manager` |
| `DISK_DRIVER`                 | The disk driver setting to be used when writing data to volumes. Valid values are, for example, `local`, `flocker`, and `cloudstor:aws`.
|                               | Typically, the disk driver is specified as `local`.  |



### Initialize Docker swarm

Docker uses swarms for cluster management and orchestration features of Docker Engine, the technology for containerizing applications. Docker engines participating in a cluster run in the swarm mode. The swarm mode is enabled by either initializing a swarm, as in the command above, or by joining an existing swarm. For more information, see [docker swarm](https://docs.docker.com/engine/reference/commandline/swarm/) and [Swarm mode key concepts](https://docs.docker.com/engine/swarm/key-concepts/) documentation.

Make sure that you initialize the Docker swarm. Log into Docker and initialize the swarm by entering the following command in a terminal:

```
docker swarm init
```

On running this command, the terminal displays information, as shown in the following example output:

```
docker swarm init --advertise-addr 192.168.99.121
Swarm initialized: current node (bvz81updecsj6wjz393c09vti) is now a manager.

To add a worker to this swarm, run the following command:

docker swarm join \
--token SWMTKN-1-3pu6hszjas19xyp7ghgosyx9k8atbfcr8p2is99znpy26u2lkl-1awxwuwd3z9j1z3puu7rcgdbx \
172.17.0.2:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```


### Open ports for Docker swarm

By default, the shell scripts provided by OpenIAM deploy to the docker swarm. You must ensure that the necessary ports are opened else the manager and worker node(s) will not be able to communicate with each other.

**Important:** Please see this information about ports after 30000 used by the swarm from the Load Balancing section of Docker documentation: The swarm manager uses ingress load balancing to expose the services you want to make available externally to the swarm. The swarm manager can automatically assign the service a PublishedPort or you can configure a PublishedPort for the service. You can specify any unused port. If you do not specify a port, the swarm manager assigns the service a port in the 30000-32767 range.


## Setup

To setup (and/or update) your configuration, you can run the setup.sh script.  This will initialize the network, and pull the latest images from dockerhub.
You can modify the script as required by your internal needs.

```
./setup.sh
```


## Teardown

You can run the teardown.sh script in order to teardown all openiam stacks, volumes, and networks.

```
./teardown.sh
```


## Startup

We provide a startup.sh script, which can be used to bring up the OpenIAM stacks.  We recommend modifying the environment variables at the top of the script, but keeping the docker commands as they are.
To startup:

```
./startup.sh
```

## Shutdown

We provide a shutdown.sh script, which can be used to shut down the OpenIAM stacks (excluding volumes).

```
./shutdown.sh
```

#### Important

Make sure that you have run the setup.sh script, and that it has executed *successfully*

Once all containers have been brought up, run the following CURL command to confirm that the back-end is indeed running:

```
curl -k -I -L http://127.0.0.1/idp/login.html
```

The return value of this curl command should contain 'HTTP/1.1 200 OK'


## Deploying multiple instances

You can easily control how many instances of a particular service you want to deploy.  Each of our services has the following entry:

```
replicas: 1
```


## Hitting the UI

Hit the following URL in your browser to view the OpenIAM UI:

```
http://127.0.0.1/webconsole
http://127.0.0.1/selfservice
```

## Elasticsearch

By default, we do not cluster Elasticsearch in docker swarm, as that requires knowledge about your environment (hostnames, etc).
Thus, by default, we run Elasticsearch in a `development` mode.

To disable this, you will have to comment out the following line in env.sh
```
export ELASTICSEARCH_DISCOVERY_TYPE="single-node"
```

However, you will then have to manually cluster Elasticsearch, which requires knowing the number of nodes (hence, horizontal scaling is impossible).

In general, for production HA deployments, we recommend our kubernetes setup:

```
https://bitbucket.org/openiam/kubernetes-docker-configuration
```

Note that we do not support Elasticsearch Authentication in docker-compose.  We support it in kubernetes only.

## Manage logging level of OpenIAM modules
To change logging level please change the `LOGGING_LEVEL` property in env.sh file:

Possible values are
- ERROR
- WARN
- INFO
- DEBUG
- TRACE  

Default value is `DEBUG`

## Running on multiple nodes

When running docker swarm on multiple nodes, it is necessary to installed a shared file system,
such as [GlusterFS](https://www.gluster.org/) if you plan on performing tasks that write to a file system

Do this on any node.

```
mkdir -p $DISTRIBUTED_FS_ROOT/sharedfs
mkdir -p $DISTRIBUTED_FS_ROOT/sync
mkdir -p $DISTRIBUTED_FS_ROOT/groovy
mkdir -p $DISTRIBUTED_FS_ROOT/activiti
mkdir -p $DISTRIBUTED_FS_ROOT/rabbitmq
mkdir -p $DISTRIBUTED_FS_ROOT/etcd
mkdir -p $DISTRIBUTED_FS_ROOT/vault
```

```
chown -R 29837:29837 $DISTRIBUTED_FS_ROOT/sharedfs
chown -R 29837:29837 $DISTRIBUTED_FS_ROOT/sync
chown -R 29837:29837 $DISTRIBUTED_FS_ROOT/groovy
chown -R 29837:29837 $DISTRIBUTED_FS_ROOT/activiti
chown -R 100:101 $DISTRIBUTED_FS_ROOT/rabbitmq
chown -R 1001 $DISTRIBUTED_FS_ROOT/etcd
chown -R 100 $DISTRIBUTED_FS_ROOT/vault
```

29837 is the default UUID and GUID in our docker containers.
101 is the default UUID for RabbitMQ, and 101 is the GUID for RabbitmQ

Then, uncomment the following sections in services/docker-compose.yaml

```
# - $DISTRIBUTED_FS_ROOT/sharedfs/:/data/openiam/conf/sharedfs
# - $DISTRIBUTED_FS_ROOT/sync/:/data/openiam/conf/sync
# - $DISTRIBUTED_FS_ROOT/groovy/:/data/openiam/conf/groovy
# - $DISTRIBUTED_FS_ROOT/activiti/:/data/openiam/conf/activiti
```

Uncomment the following sections in infrastructure/rabbitmq/docker-compose.yaml

```
# - $DISTRIBUTED_FS_ROOT/rabbitmq/:/var/lib/rabbitmq
```

Uncomment the following sections in infrastructure/etcd/docker-compose.yaml

```
# - $DISTRIBUTED_FS_ROOT/etcd/:/opt/bitnami/etcd/data
```

Uncomment the following sections in :

1) infrastructure/vault/docker-compose.yaml
2) infrastructure/mariadb/docker-compose.yaml
3) infrastructure/redis/docker-compose.yaml
4) infrastructure/rabbitmq/docker-compose.yaml

```
# - $DISTRIBUTED_FS_ROOT/vault/server:/data/openiam/conf/vault/server
```

Uncomment the following sections in services/docker-compose.yaml

```
# - $DISTRIBUTED_FS_ROOT/vault/client:/data/openiam/conf/vault/client
```

Replace $DISTRIBUTED_FS_ROOT with the root of your distributed file system

You will have to redeploy the service after doing this, if you've already started the cluster

```
./startup.sh
```

## Default User Mappings

The following UUID/GUID are used in our docker containers:

````
mysql container -> mysql:x:999:999::/home/mysql:/bin/sh
redis container -> redis:x:100:101:Linux User,,,:/home/redis:/bin/false
elasticsearch contianer -> uid=1000(elasticsearch) gid=1000(elasticsearch) groups=1000(elasticsearch),0(root)
rabbitmq containers -> rabbitmq:x:100:101:Linux User,,,:/var/lib/rabbitmq:/bin/false
openiam containers -> openiam:x:29837:29837:Linux User,,,:/home/openiam:/sbin/halt
etcd  containers -> 1001 (has no username)
vault container -> vault:x:100:1000:Linux User,,,:/home/vault:/bin/false
````


## Syslogs
For redirect system logs to docker host we use variables :
`SYS_LOG_HOST`  - default value is "host.openiam"
`DOCKER_HOST_IP` - the IP of the Docker Host

New host name will be added to /etc/hosts  for esb docker container. This host name we will use in OpenIAM syslog settings.

## Redisson configuration

## Response timeout

If required you are able to change response timeout on redisson. Change the following property
in env.sh

````
export REDISSON_TIMEOUT="5000"
````

This may fix problem with redis timeout exception, for example, like below

````
        org.redisson.client.RedisTimeoutException: Redis server response timeout (3000 ms) occured for command: (HINCRBYFLOAT) with params: [RUNNING_SYNC, [B@1e6f24d, 0] channel: [id: 0x3d06da6b, L:/10.0.2.33:34504 - R:redis/10.0.2.17:6379]
    	at org.redisson.command.CommandAsyncService$10.run(CommandAsyncService.java:630) ~[redisson-2.5.1.jar!/:na]
    	at io.netty.util.HashedWheelTimer$HashedWheelTimeout.expire(HashedWheelTimer.java:588) ~[netty-common-4.0.42.Final.jar!/:4.0.42.Final]
    	at io.netty.util.HashedWheelTimer$HashedWheelBucket.expireTimeouts(HashedWheelTimer.java:662) ~[netty-common-4.0.42.Final.jar!/:4.0.42.Final]
    	at io.netty.util.HashedWheelTimer$Worker.run(HashedWheelTimer.java:385) ~[netty-common-4.0.42.Final.jar!/:4.0.42.Final]
    	at java.lang.Thread.run(Thread.java:748) ~[na:1.8.0_171]
````


## Logging Healthchecks

There are instances when you may want to log the results of the healthcheck.  This ability has been added as of 4.1.4.3

1) Ensure that you have trace enabled.

```
    environment:
      OPENIAM_LOG_LEVEL: trace
```

2) Pipe the result of the healthcheck to a file.  Note that this file will be created *on the container*
Thus,if you want to persist it, you will have to map it to a volume.

```
    healthcheck:
      test: "(bash -c 'healthcheck.sh 120 2>> /tmp/health' && exit 0) || exit 1"
```


## Upgrading


### Updating to 4.1.4.3

If you are using our mariadb container, and you are to 4.1.4.3 (or later),
you will have to run a few manual commands (as root) to fix a permissions issue.

First, inspect the openiam-db-storage_storage volume

```
[root@qa3 openiam-docker-compose]# docker volume inspect openiam-db-storage_storage
[
    {
        "CreatedAt": "2018-11-27T10:01:20-05:00",
        "Driver": "local",
        "Labels": {
            "com.docker.stack.namespace": "openiam-db-storage"
        },
        "Mountpoint": "/var/lib/docker/volumes/openiam-db-storage_storage/_data",
        "Name": "openiam-db-storage_storage",
        "Options": {},
        "Scope": "local"
    }
]
```

Next, run the command to fix the permissions issue
```
chown -R 999:999 /var/lib/docker/volumes/openiam-db-storage_storage/_data/*
```

### Updating to 4.2.1.9

In 4.2.1.9 we've updated the RabbitMQ queue types to be resilient for HA.  This means that a manual step must be performed after upgrading.

1) Get the latest `RELEASE-4.2.1.9` of this project
2) `./shutdown.sh`
3) `docker rm ($docker ps -a -q) -f`
4) `docker volume ls | grep rabbitmq`
5) `docker volume rm <all from the above ouptut>`
6) `./startup.sh`
