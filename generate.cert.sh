#!/usr/bin/env bash

set -e

. env.sh

if [ -f /opt/openiam/webapps/env.sh ]
then
  . /opt/openiam/webapps/env.sh
fi

if [ ! -z "$OPENIAM_LOG_LEVEL" ] && [ "$OPENIAM_LOG_LEVEL" == "debug" ] || [ "$OPENIAM_LOG_LEVEL" == "trace" ]
then
  set -x
fi

VAULT_URL=vault

if [ -z "$CIRCLECI" ]; then
	echo "This script will generate a keypair that vault will use.  Make sure to first set VAULT_JKS_PASSWORD in env.sh"
	read -p "Press enter to continue"
fi

if [ -z "VAULT_JKS_PASSWORD" ] || [ "VAULT_JKS_PASSWORD" == "null" ]; then
	echo "VAULT_JKS_PASSWORD must be set in env.sh"
	exit 1;
fi


## create certs here
openssl genrsa -out vault.ca.key 2048
openssl req -new -x509 -days 3650 -key vault.ca.key -out vault.ca.crt -subj "/C=CZ/ST=Test/L=Test/O=Test/OU=Test/CN=${VAULT_URL}"
echo -n "00" > vault.file.srl

openssl genrsa -out vault.key 2048
openssl req -new -key vault.key -out vault.csr -subj "/C=CZ/ST=Test/L=Test/O=Test/OU=Test/CN=${VAULT_URL}"

openssl x509 -req -days 3650 -in vault.csr -CA vault.ca.crt -CAkey vault.ca.key -CAserial vault.file.srl -out vault.crt
openssl pkcs12 -export -clcerts -in vault.crt -inkey vault.key -out vault.p12 -password pass:${VAULT_JKS_PASSWORD}
openssl rsa -in vault.key -out vault.no_pem.key

openssl pkcs12 -export -in vault.crt -inkey vault.key -out vault.jks -password pass:${VAULT_JKS_PASSWORD}
