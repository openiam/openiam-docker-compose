#!/usr/bin/env bash

set -x
set -e

. env.sh

if [ -f /opt/openiam/webapps/env.sh ]
then
  . /opt/openiam/webapps/env.sh
fi

if [ -z "${OPENIAM_RPROXY_HTTP}" ]; then
    echo "set OPENIAM_RPROXY_HTTP to 1 (recommended) or 0 (not recommended, unless you have load balancer with https in front of rproxy)"
    exit 1
fi


if [ "${OPENIAM_RPROXY_HTTP}" -eq 0 ]; then
if [ -z "${OPENIAM_SSL_CERT}" ]; then
    if [ -z "${OPENIAM_SSL_CHAIN}" ]; then
        echo "set OPENIAM_SSL_CERT or OPENIAM_SSL_CHAIN to correct path to ssl certificate or ssl certificates chain"
        exit 1
    fi
fi

if [ -z "${OPENIAM_SSL_CERT_KEY}" ]; then
    echo "set OPENIAM_SSL_CERT_KEY_PATH to correct path to ssl certificate private key"
    exit 1
fi
fi

# configure docker networks
. /etc/os-release

if  [ "$VERSION_CODENAME" == "jammy" ]; then
    if [ "$(systemctl is-active ufw)" == "active" ]; then
        echo "Docker and ufw use iptables in ways that make them incompatible with each other. Disable UFW" 
        systemctl stop ufw
        systemctl disable ufw
    fi
fi

if [[ ! "$(docker network ls | grep openiam_private)" ]]; then
    docker network create --attachable  --driver=overlay openiam_private
    sleep 5
fi

docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/flyway:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/esb:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/workflow:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/idm:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/groovy-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/business-rule-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/synchronization:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/email-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/device-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/auth-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
# deploy sas-manager module
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/sas-manager:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"


if [[ "$DB_TYPE" == 'MariaDB' ]]; then
    docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/mariadb:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
fi

if [[ "$DB_TYPE" == 'Postgres' ]]; then
    docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/postgres:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
fi


docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/reconciliation:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/redis:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/janusgraph:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/elasticsearch:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/rabbitmq:alpine-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/openiam-metadata:alpine-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#connectors
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/ldap-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/google-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/linux-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/oracle-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/scim-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/aws-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/script-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/oracle-ebs-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/freshdesk-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/oracle-idcs-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/tableau-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/adp-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/ipa-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/salesforce-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/jdbc-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/box-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/kronos-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/workday-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/workday-rest-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/boomi-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/lastpass-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/thales-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/thales-wsdl-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/postgresql-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/rexx-connector-rabbitmq:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#rproxy
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/rproxy:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
#source adapter. commented due to this is optional
#docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/http-source-adapter:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"

#data analyzing tool
 if [[ "OPENIAM_MONITORING_TOOLS_ENABLED" -eq 1 ]]; then
    docker pull "docker.elastic.co/beats/metricbeat:${OPENIAM_MONITORING_TOOLS_VERSION}"
    docker pull "docker.elastic.co/beats/filebeat:${OPENIAM_MONITORING_TOOLS_VERSION}"
    docker pull "docker.elastic.co/kibana/kibana:${OPENIAM_MONITORING_TOOLS_VERSION}"
fi

if [ "$ELASTICDUMP_ENABLED" == "true" ]; then
  docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/elasticdump:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
fi

docker pull bitnami/etcd:3.3.13
docker pull bitnami/cassandra:4.0.11
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/vault:alpine-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
docker pull "${CONTAINER_REGISTRY}/${CONTAINER_INFRA_NAMESPACE}/vault-bootstrap:alpine-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"

docker pull "${CONTAINER_REGISTRY}/${CONTAINER_SERVICE_NAMESPACE}/ui:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"
